using UnitySocketShared.Base.Global;

namespace UnitySocketReceiver.Network
{
    public class SocketActions
    {
        public static void Init()
        {
            GlobalStaticVariables.ClientActions.Add("SetData", (info, args) =>
            {
                if (args == null) return;
                if (args.Count < 2) return;    // 인자의 수가 부족하다.
                var dataName = args[0] as string;
                var data = args[1];

                if (dataName == null) return;  // 데이터의 이름이 null일 경우 무시
                info.SetData(dataName, data);
            });
        }
    }
}