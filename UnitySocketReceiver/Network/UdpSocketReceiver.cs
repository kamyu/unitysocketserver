using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Snappy.Sharp;
using UnitySocketShared.Base.Classes;
using UnitySocketShared.Base.Global;
using UnitySocketReceiver.Utilities;

namespace UnitySocketReceiver.Network
{
    public class UdpSocketReceiver
    {
        private static Thread _thread;
        
        public static void Init()
        {
            _thread = new Thread(_ =>
            {
                while (true)
                {
                    if (!GlobalStaticVariables.TcpClient.connected) continue;
                    if (GlobalStaticVariables.TcpClient.socket == null) continue;
                    if (GlobalStaticVariables.TcpClient.sessionId == null ||
                        string.IsNullOrWhiteSpace(GlobalStaticVariables.TcpClient.sessionId)) continue;
                    
                    // TCP가 연결되어있지 않다면 소켓을 생성하지 않고 반복한다.
                    
                    GlobalStaticVariables.UdpClient.socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    var udpClient = GlobalStaticVariables.UdpClient;
                    var hostAddresses = Dns.GetHostAddresses(GlobalStaticVariables.connectAddress);
                    if (hostAddresses.Length <= 0) continue;

                    udpClient.sessionId = GlobalStaticVariables.TcpClient.sessionId;
                    udpClient.ip = hostAddresses[0].ToString();
                    udpClient.port = GlobalStaticVariables.UdpPort;
                    break;
                }
                
                // 소켓이 생성된 뒤로 서버가 열려있는지 확인한다.
                while (true)
                {
                    if (GlobalStaticVariables.UdpClient.connected) break;
                    
                    try
                    {
                        // 데이터를 전송한다. - 만약 에러가 발생할경우 false 를 반환한다.
                        GlobalStaticVariables.UdpClient.SendData();
                        GlobalStaticVariables.UdpClient.connected = true;
                    }
                    catch (Exception e)
                    {
                        ActionQueue.DebugLog(e.ToString());
                        GlobalStaticVariables.UdpClient.connected = false;
                    }
                }
                
                // 서버가 연결되면 실시간으로 데이터를 받는다.
                while (true)
                {
                    if (!GlobalStaticVariables.UdpClient.connected) continue;
                    if (GlobalStaticVariables.UdpClient.socket == null) continue;

                    if (!GlobalStaticVariables.UdpClient.socket.Poll(0, SelectMode.SelectRead)) continue;

                    try
                    {
                        var bytes = new byte[GlobalStaticVariables.DefaultByteSize];
                        EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                        var byteSize = GlobalStaticVariables.UdpClient.socket.ReceiveFrom(bytes, SocketFlags.None, ref remoteEndPoint);
                        
                        if (byteSize > 0)
                        {
                            Array.Resize(ref bytes, byteSize);
                            
                            var ms = new MemoryStream();
                            using (var dms = new MemoryStream(bytes))
                            { using (var ds = new DeflateStream(dms, CompressionMode.Decompress)) { ds.CopyTo(ms); ds.Close(); } dms.Close(); }
                            
                            var decompressedBytes = ms.ToArray();
                            decompressedBytes = new SnappyDecompressor().Decompress(decompressedBytes, 0, decompressedBytes.Length);
                            
                            Array.Clear(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                            ms.Position = 0;
                            ms.SetLength(0);
                            
                            var binFormatter = new BinaryFormatter {Binder = new AllowAllAssemblyVersionsDeserializationBinder()};
                            ms.Write(decompressedBytes, 0, decompressedBytes.Length);
                            ms.Position = 0;

                            if (!(binFormatter.Deserialize(ms) is Dictionary<string, object> readData))
                                throw new Exception("Data is not dictionary");
                            ms.Close();
                            
                            if (!readData.ContainsKey("clientInfo")) throw new Exception("clientInfo is Null");
                            if (!(readData["clientInfo"] is ClientInfo clientInfo))
                                throw new Exception("clientInfo is not ClientInfo Class");
                            
                            // 데이터가 있는지 확인한다
                            if (clientInfo.sessionId == null ||
                                string.IsNullOrWhiteSpace(clientInfo.sessionId))
                                throw new Exception("sessionId is Null");
                            if (clientInfo.ip == null || string.IsNullOrWhiteSpace(clientInfo.ip))
                                throw new Exception("ip is Null");
                            if (clientInfo.port == 0) throw new Exception("port is 0");
                            
                            ActionQueue.DebugLog("[UDP] Receive Data - " + byteSize);
                            
                            // 요청한 액션이 있을 경우
                            if (!readData.ContainsKey("actions")) continue;
                            if (!(readData["actions"] is Dictionary<string, List<object>> actions)) continue;
                            foreach (var action in actions)
                            {
                                if (!GlobalStaticVariables.ClientActions.ContainsKey(action.Key)) continue;
                                GlobalStaticVariables.ClientActions[action.Key]
                                    ?.Invoke(GlobalStaticVariables.UdpClient,
                                        action.Value);
                            }
                        }
                        else
                        {
                            throw new Exception("[Udp] Server Closed.");
                        }
                    }
                    catch (Exception e)
                    {
                        ActionQueue.DebugLog(e.ToString());
                        
                        GlobalStaticVariables.TcpClient.connected = false;
                        ActionQueue.ApplicationQuit();
                    }
                }
            });
            _thread.Start();
        }
    }
}