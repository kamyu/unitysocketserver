using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Snappy.Sharp;
using UnitySocketShared.Base.Classes;
using UnitySocketShared.Base.Global;
using UnitySocketReceiver.Utilities;

namespace UnitySocketReceiver.Network
{
    public class TcpSocketReceiver
    {
        private static Thread _thread;
        
        public static void Init()
        {
            _thread = new Thread(_ =>
            {
                while (true)
                {
                    if (GlobalStaticVariables.TcpClient.socket == null)
                    {
                        GlobalStaticVariables.TcpClient.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        var clientSocket = GlobalStaticVariables.TcpClient.socket;
                        var hostAddresses = Dns.GetHostAddresses(GlobalStaticVariables.connectAddress);
                        if (hostAddresses.Length <= 0) continue;
                        
                        var asyncResult = clientSocket.BeginConnect(hostAddresses, GlobalStaticVariables.TcpPort, null, null);
                        if (!asyncResult.AsyncWaitHandle.WaitOne(GlobalStaticVariables.ConnectTimeout, false)) continue;
                        
                        GlobalStaticVariables.TcpClient.connected = true;
                        
                        clientSocket.EndConnect(asyncResult);
                    }
                    else if (!GlobalStaticVariables.TcpClient.connected)
                    {
                        GlobalStaticVariables.TcpClient.socket.Dispose();
                        GlobalStaticVariables.TcpClient.socket.Close();
                        GlobalStaticVariables.TcpClient.socket = null;
                        GlobalStaticVariables.TcpClient.connected = false;
                        continue;
                    }

                    break;
                }

                if (!GlobalStaticVariables.TcpClient.connected) return;
                if (GlobalStaticVariables.TcpClient.socket == null) return;
                
                // 서버에 연결이 될경우 무조건 데이터를 한번 받아야 한다.
                {
                    var bytes = new byte[GlobalStaticVariables.DefaultByteSize];
                    var byteSize = GlobalStaticVariables.TcpClient.socket.Receive(bytes, SocketFlags.None);

                    // 받은 배열의 크기만큼 배열의 크기를 조절한다
                    Array.Resize(ref bytes, byteSize);

                    var ms = new MemoryStream();
                    using (var dms = new MemoryStream(bytes))
                    {
                        using (var ds = new DeflateStream(dms, CompressionMode.Decompress))
                        {
                            ds.CopyTo(ms);
                            ds.Close();
                        }
                        dms.Close();
                    }

                    // 데이터의 압축을 푼다
                    var deflateBytes = ms.ToArray();
                    Array.Clear(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                    ms.Position = 0;
                    ms.SetLength(0);
                    
                    var snappyBytes = new SnappyDecompressor().Decompress(deflateBytes, 0, deflateBytes.Length);

                    // 첫 데이터를 받을시 로그를 출력한다
                    ActionQueue.DebugLog("[TCP] ReceiveData - " + bytes.Length);

                    var binFormatter = new BinaryFormatter {Binder = new AllowAllAssemblyVersionsDeserializationBinder()};
                    ms.Write(snappyBytes, 0, snappyBytes.Length);
                    ms.Position = 0;

                    if (!(binFormatter.Deserialize(ms) is Dictionary<string, object> readData))
                        throw new Exception("Data is not dictionary");
                    ms.Close();

                    if (!readData.ContainsKey("clientInfo")) throw new Exception("clientInfo is Null");
                    if (!(readData["clientInfo"] is ClientInfo receiveClientInfo))
                        throw new Exception("clientInfo is not ClientInfo Class");

                    // 데이터를 설정한다.
                    GlobalStaticVariables.TcpClient.sessionId = receiveClientInfo.sessionId;
                    GlobalStaticVariables.TcpClient.ip = receiveClientInfo.ip;
                    GlobalStaticVariables.TcpClient.port = receiveClientInfo.port;
                }
                
                // 데이터를 실시간으로 받는다.
                while (true)
                {
                    if (!GlobalStaticVariables.TcpClient.connected) continue;
                    if (GlobalStaticVariables.TcpClient.socket == null) continue;

                    if (!GlobalStaticVariables.TcpClient.socket.Poll(0, SelectMode.SelectRead)) continue;

                    try
                    {
                        var bytes = new byte[GlobalStaticVariables.DefaultByteSize];
                        var byteSize = GlobalStaticVariables.TcpClient.socket.Receive(bytes, SocketFlags.None);
                        if (byteSize > 0)
                        {
                            Array.Resize(ref bytes, byteSize);
                            
                            var ms = new MemoryStream();
                            using (var dms = new MemoryStream(bytes))
                            { using (var ds = new DeflateStream(dms, CompressionMode.Decompress)) { ds.CopyTo(ms); ds.Close(); } dms.Close(); }
                            
                            var decompressedBytes = ms.ToArray();
                            decompressedBytes = new SnappyDecompressor().Decompress(decompressedBytes, 0, decompressedBytes.Length);
                            
                            Array.Clear(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                            ms.Position = 0;
                            ms.SetLength(0);
                            
                            var binFormatter = new BinaryFormatter {Binder = new AllowAllAssemblyVersionsDeserializationBinder()};
                            ms.Write(decompressedBytes, 0, decompressedBytes.Length);
                            ms.Position = 0;

                            if (!(binFormatter.Deserialize(ms) is Dictionary<string, object> readData))
                                throw new Exception("Data is not dictionary");
                            ms.Close();
                            
                            if (!readData.ContainsKey("clientInfo")) throw new Exception("clientInfo is Null");
                            if (!(readData["clientInfo"] is ClientInfo clientInfo))
                                throw new Exception("clientInfo is not ClientInfo Class");
                            
                            // 데이터가 있는지 확인한다
                            if (clientInfo.sessionId == null ||
                                string.IsNullOrWhiteSpace(clientInfo.sessionId))
                                throw new Exception("sessionId is Null");
                            if (clientInfo.ip == null || string.IsNullOrWhiteSpace(clientInfo.ip))
                                throw new Exception("ip is Null");
                            if (clientInfo.port == 0) throw new Exception("port is 0");
                            
                            ActionQueue.DebugLog("[TCP] Receive Data - " + byteSize);
                            
                            // 요청한 액션이 있을 경우
                            if (!readData.ContainsKey("actions")) continue;
                            if (!(readData["actions"] is Dictionary<string, List<object>> actions)) continue;
                            foreach (var action in actions)
                            {
                                if (!GlobalStaticVariables.ClientActions.ContainsKey(action.Key)) continue;
                                GlobalStaticVariables.ClientActions[action.Key]
                                    ?.Invoke(GlobalStaticVariables.TcpClient,
                                        action.Value);
                            }
                        }
                        else
                        {
                            throw new Exception("[TCP] Server Closed.");
                        }
                    }
                    catch (Exception e)
                    {
                        ActionQueue.DebugLog(e.ToString());
                        
                        GlobalStaticVariables.TcpClient.connected = false;
                        ActionQueue.ApplicationQuit();
                    }
                }
            });
            _thread.Start();
        }
    }
}