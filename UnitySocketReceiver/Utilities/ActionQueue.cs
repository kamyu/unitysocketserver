using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnitySocketReceiver.Utilities
{
    public class ActionQueue : MonoBehaviour
    {
        private static readonly Queue<Action> Actions = new Queue<Action>();

        private void Update()
        {
            if (Actions.Count <= 0) return;
            
            var dequeue = Actions.Dequeue();
            dequeue?.Invoke();
        }

        public static void AddAction(Action action)
        {
            if (action == null) return;
            Actions.Enqueue(action);
        }

        public static void DebugLog(string str)
        {
            AddAction(() => { Debug.Log(str); });
        }

        public static void ApplicationQuit()
        {
            AddAction(UnityEngine.Application.Quit);
        }

        // singleton
        public static ActionQueue Instance = null;
        [RuntimeInitializeOnLoadMethod]
        public static void Create()
        {
            while (true)
            {
                if (Instance != null) return;

                var obj = new GameObject();
                var aq = obj.AddComponent<ActionQueue>();
                if (aq == null)
                {
                    Destroy(obj);
                    continue;
                }

                obj.name = aq.GetType().FullName ?? "Unknown Type";
                DontDestroyOnLoad(obj);
                break;
            }
        }
    }
}