using System;
using UnityEngine;
using UnitySocketReceiver.Network;

namespace UnitySocketReceiver
{
    public class SocketReceiverMain : MonoBehaviour
    {
        private void Start()
        {
            if (Instance == null) return;
            Console.WriteLine("Server Init");
            
            TcpSocketReceiver.Init();
            UdpSocketReceiver.Init();
            SocketActions.Init();
        }

        // singleton
        public static SocketReceiverMain Instance;
        
        [RuntimeInitializeOnLoadMethod]
        public static void Create()
        {
            Console.WriteLine("Socket Receiver Main Create");
            
            while (true)
            {
                if (Instance != null) break;

                var obj = new GameObject();
                Instance = obj.AddComponent<SocketReceiverMain>();
                if (Instance == null)
                {
                    Console.WriteLine("Socket Receiver Main Delete");
                    Destroy(obj);
                    continue;
                }

                obj.name = Instance.GetType().FullName ?? "Unknown Type";
                DontDestroyOnLoad(obj);
                
                Debug.Log("Create");
                break;
            }
        }
    }
}