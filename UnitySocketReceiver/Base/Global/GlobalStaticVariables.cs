using System;
using System.Collections.Generic;
using System.Net.Sockets;
using UnitySocketShared.Base.Classes;

namespace UnitySocketShared.Base.Global
{
    public static class GlobalStaticVariables
    {
        public const string connectAddress = "kamyu.pw";
        public const int TcpPort = 9000;
        public const int UdpPort = 9001;
        public const int DefaultByteSize = 1024;
        public const int ConnectTimeout = 5;
        
        public static ClientInfo TcpClient = new ClientInfo { sessionId = "", pType = ProtocolType.Tcp };
        public static ClientInfo UdpClient = new ClientInfo { sessionId = "", pType = ProtocolType.Udp };
        
        public static readonly Dictionary<string, Action<ClientInfo, List<object>>> ClientActions = new Dictionary<string, Action<ClientInfo, List<object>>>();
        public static readonly Dictionary<string, object> ClientData = new Dictionary<string, object>();
    }
}