using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Snappy.Sharp;
using UnitySocketShared.Base.Classes;
using UnitySocketServer.Base.Command;
using UnitySocketShared.Base.Global;

namespace UnitySocketServer.Network
{
    public static class TcpSocketReceive
    {
        private static Thread _thread;
        private static readonly object ThreadLockObject = new object();

        public static void Init()
        {
            _thread = new Thread(_ =>
            {
                var serverSocket = TcpSocketAuth.MainSocket;
                while (true)
                {
                    if (serverSocket == null) continue;
                    if (GlobalStaticVariables.TcpClientList == null) continue;

                    lock (ThreadLockObject)
                    {
                        foreach (var client in GlobalStaticVariables.TcpClientList)
                        {
                            try
                            {

                                if (client.Value == null || !client.Value.connected || client.Value.socket == null)
                                    throw new Exception("Client is Null");

                                var clientInfo = client.Value;
                                if (!clientInfo.socket.Poll(0, SelectMode.SelectRead)) continue;

                                var bytes = new byte[GlobalStaticVariables.DefaultByteSize];
                                var byteSize = clientInfo.socket.Receive(bytes, SocketFlags.None);

                                if (byteSize > 0)
                                {
                                    Array.Resize(ref bytes, byteSize);

                                    var ms = new MemoryStream();
                                    using (var dms = new MemoryStream(bytes))
                                    {
                                        using (var ds = new DeflateStream(dms, CompressionMode.Decompress))
                                        {
                                            ds.CopyTo(ms);
                                            ds.Close();
                                        }

                                        dms.Close();
                                    }

                                    var deflateBytes = ms.ToArray();

                                    Array.Clear(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                                    ms.Position = 0;
                                    ms.SetLength(0);

                                    var snappyBytes =
                                        new SnappyDecompressor().Decompress(deflateBytes, 0, deflateBytes.Length);

                                    var binFormatter = new BinaryFormatter {Binder = new AllowAllAssemblyVersionsDeserializationBinder()};
                                    ms.Write(snappyBytes, 0, snappyBytes.Length);
                                    ms.Position = 0;

                                    if (!(binFormatter.Deserialize(ms) is Dictionary<string, object> readData))
                                        throw new Exception("Data is not dictionary");
                                    ms.Close();

                                    if (!readData.ContainsKey("clientInfo")) continue;
                                    if (!(readData["clientInfo"] is ClientInfo receiveClientInfo))
                                        throw new Exception("clientInfo is not ClientInfo Class");

                                    // 데이터가 있는지 확인한다
                                    if (receiveClientInfo.sessionId == null ||
                                        string.IsNullOrWhiteSpace(receiveClientInfo.sessionId))
                                        throw new Exception("sessionId is Null");
                                    if (receiveClientInfo.ip == null || string.IsNullOrWhiteSpace(receiveClientInfo.ip))
                                        throw new Exception("ip is Null");
                                    if (receiveClientInfo.port == 0) throw new Exception("port is 0");

                                    // 데이터가 일치하는지 확인한다
                                    if (clientInfo.sessionId != receiveClientInfo.sessionId)
                                        throw new Exception("sessionId is not correct.");
                                    if (clientInfo.ip != receiveClientInfo.ip)
                                        throw new Exception("ip is not correct.");
                                    if (clientInfo.port != receiveClientInfo.port)
                                        throw new Exception("port is not correct.");

                                    Console.WriteLine("[TCP] Receive Data - " + byteSize);

                                    // 요청한 액션이 있을 경우
                                    if (!readData.ContainsKey("actions")) continue;
                                    if (!(readData["actions"] is Dictionary<string, List<object>> actions)) continue;
                                    foreach (var action in actions)
                                    {
                                        if (!GlobalStaticVariables.ServerActions.ContainsKey(action.Key)) continue;
                                        GlobalStaticVariables.ServerActions[action.Key]
                                            ?.Invoke(GlobalStaticVariables.TcpClientList[clientInfo.sessionId],
                                                action.Value);
                                    }
                                }
                                else
                                {
                                    throw new Exception("Client Disconnect - " + client.Key);
                                }

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);

                                if (GlobalStaticVariables.TcpClientList.ContainsKey(client.Key))
                                {
                                    GlobalStaticVariables.TcpClientList.Remove(client.Key);
                                    Console.WriteLine("[TCP] Remove Client - " + client.Key);
                                }

                                if (GlobalStaticVariables.UdpClientList.ContainsKey(client.Key))
                                {
                                    GlobalStaticVariables.UdpClientList.Remove(client.Key);
                                    Console.WriteLine("[UDP] Remove Client - " + client.Key);
                                }

                                break;
                            }
                        }
                    }

                    Thread.Sleep(10);
                }
            });
            _thread.Start();

            // 디버깅용 멍령어
            ConsoleCommand.AddCommand("tcpsendtest", "", args =>
            {
                if (TcpSocketAuth.MainSocket == null) return;

                foreach (var client in GlobalStaticVariables.TcpClientList)
                {
                    client.Value.SendData();
                }
            });
            
            // 데이터 설정 정보를 받았을 경우
            
        }
    }
}