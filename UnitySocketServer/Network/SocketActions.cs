using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnitySocketShared.Base.Classes;
using UnitySocketShared.Base.Global;

namespace UnitySocketServer.Network
{
    public class SocketActions
    {
        public static void Init()
        {
            GlobalStaticVariables.ServerActions.Add("SetData", (info, args) =>
            {
                if (args == null) return;
                if (args.Count < 2) return;    // 인자의 수가 부족하다.
                var dataName = args[0] as string;
                var data = args[1];

                if (dataName == null) return;  // 데이터의 이름이 null일 경우 무시
                info.SetData(dataName, data);
            });
            
            GlobalStaticVariables.ServerActions.Add("SpawnObject", (info, args) =>
            {
                if (args == null) return;
                if (args.Count < 2) return;
                var objectInstanceId = args[0] as string;
                var objectTypeText = args[1] as string;

                if (string.IsNullOrWhiteSpace(objectInstanceId)) return;
                if (string.IsNullOrWhiteSpace(objectTypeText)) return;
                
                if(!GlobalStaticVariables.ServerData.ContainsKey("SyncObjects")) GlobalStaticVariables.ServerData.Add("SyncObjects", new Dictionary<string, SyncGameObject>());
                if (!(GlobalStaticVariables.ServerData["SyncObjects"] is Dictionary<string, SyncGameObject> syncGameObjects)) return;
                
                string objectNetworkName;
                using (var sha1 = new SHA1CryptoServiceProvider()) objectNetworkName = Convert.ToBase64String(sha1.ComputeHash(Encoding.ASCII.GetBytes(DateTime.Now.ToString("yyyyMMddHHmmss") + objectInstanceId.ToString())));

                var syncObject = new SyncGameObject
                {
                    objectInstanceId = objectInstanceId,
                    objectNetworkName = objectNetworkName
                };

                syncGameObjects.Add(objectNetworkName, syncObject);
                
                info.SendData(new Dictionary<string, List<object>>
                {
                    {
                        "SpawnObject",
                        new List<object> {syncObject}
                    }
                });
                
            });
            
            GlobalStaticVariables.ServerActions.Add("DestroyObject", (info, args) =>
            {
                if (args == null) return;
                if (args.Count < 2) return;
                var objectInstanceId = args[0] as string;
                var objectNetworkName = args[1] as string;

                if (string.IsNullOrWhiteSpace(objectInstanceId)) return;
                if (string.IsNullOrWhiteSpace(objectNetworkName)) return;
                
                if (!GlobalStaticVariables.ServerData.ContainsKey("SyncObjects")) GlobalStaticVariables.ServerData.Add("SyncObjects", new Dictionary<string, SyncGameObject>());
                if (!(GlobalStaticVariables.ServerData["SyncObjects"] is Dictionary<string, SyncGameObject> syncGameObjects)) return;

                var syncObject = syncGameObjects[objectNetworkName];
                if (syncObject == null) return;
                
                info.SendData(new Dictionary<string, List<object>>
                {
                    {
                        "DestroyObject",
                        new List<object> {syncObject}
                    }
                });
                
            });
        }
    }
}