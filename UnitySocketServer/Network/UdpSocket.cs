using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Snappy.Sharp;
using UnitySocketShared.Base.Classes;
using UnitySocketShared.Base.Global;

namespace UnitySocketServer.Network
{
    public static class UdpSocket
    {
        public static Socket MainSocket;
        private static Thread _acceptThread;
        private static readonly object ThreadLockObject = new object();

        public static void Init()
        {
            Console.WriteLine("Udp Socket Auth Init");
            
            MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            MainSocket.Bind(new IPEndPoint(IPAddress.Any, GlobalStaticVariables.UdpPort));

            void Start(object _)
            {
                while (true)
                {
                    try
                    {
                        if (!MainSocket.Poll(0, SelectMode.SelectRead)) continue;
                        
                        var bytes = new byte[GlobalStaticVariables.DefaultByteSize];
                        EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                        var byteSize = MainSocket.ReceiveFrom(bytes, ref remoteEndPoint);

                        if (byteSize <= 0) continue;
                        
                        Array.Resize(ref bytes, byteSize);
                        
                        var ms = new MemoryStream();
                        using (var dms = new MemoryStream(bytes))
                        {
                            using (var ds = new DeflateStream(dms, CompressionMode.Decompress))
                            {
                                ds.CopyTo(ms);
                                ds.Close();
                            }
                            dms.Close();
                        }
                        
                        var deflateBytes = ms.ToArray();
                        
                        Array.Clear(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                        ms.Position = 0;
                        ms.SetLength(0);
                        
                        var snappyBytes = new SnappyDecompressor().Decompress(deflateBytes, 0, deflateBytes.Length);
                        var binFormatter = new BinaryFormatter {Binder = new AllowAllAssemblyVersionsDeserializationBinder()};
                        ms.Write (snappyBytes, 0, snappyBytes.Length);
                        ms.Position = 0;
                        
                        var ipEndPoint = remoteEndPoint as IPEndPoint;
                        if (ipEndPoint == null) throw new Exception("ipEndPoint is Null");
                        var ip = ipEndPoint.Address.ToString();
                        var port = ipEndPoint.Port;
                        
                        if(!(binFormatter.Deserialize(ms) is Dictionary<string, object> readData)) throw new Exception("Data is not dictionary");

                        ms.Close();
                        
                        if (!readData.ContainsKey("clientInfo")) continue;
                        if(!(readData["clientInfo"] is ClientInfo clientInfo)) throw new Exception("clientInfo is not ClientInfo Class");
                            
                        // 데이터가 있는지 확인한다
                        if(clientInfo.sessionId == null || string.IsNullOrWhiteSpace(clientInfo.sessionId)) throw new Exception("sessionId is Null");
                        if(clientInfo.ip == null || string.IsNullOrWhiteSpace(clientInfo.ip)) throw new Exception("ip is Null");
                        if(clientInfo.port == 0) throw new Exception("port is 0");
                        
                        // Udp 목록에 클라이언트가 없을 경우 클라이언트 정보를 등록한다
                        if (!GlobalStaticVariables.UdpClientList.ContainsKey(clientInfo.sessionId))
                        {
                            lock (ThreadLockObject)
                            {
                                var newClientInfo = new ClientInfo
                                {
                                    sessionId = clientInfo.sessionId,
                                    connected = true,
                                    datas = new Dictionary<string, object>(),
                                    ip = ip,
                                    port = port,
                                    pType = ProtocolType.Udp,
                                    socket = MainSocket
                                };
                                GlobalStaticVariables.UdpClientList.Add(clientInfo.sessionId, newClientInfo);
                                Console.WriteLine("[UDP] Add Client (" + ip + ":" + port + ") - " + clientInfo.sessionId);
                                newClientInfo.SendData();
                            }
                        }

                        var savedClientInfo = GlobalStaticVariables.UdpClientList[clientInfo.sessionId];
                        
                        // 데이터가 일치하는지 확인한다
                        if(clientInfo.sessionId != savedClientInfo.sessionId) throw new Exception("sessionId is not correct.");
                        if(ip != savedClientInfo.ip) throw new Exception("ip is not correct.");
                        if(port != savedClientInfo.port) throw new Exception("port is not correct.");
                        
                        Console.WriteLine("[UDP] Receive Data - " + byteSize);
                        
                        // 요청한 액션이 있을 경우
                        if (!readData.ContainsKey("actions")) continue;
                        if (!(readData["actions"] is Dictionary<string, List<object>> actions)) continue;
                        foreach (var action in actions)
                        {
                            if (!GlobalStaticVariables.ServerActions.ContainsKey(action.Key)) continue;
                            GlobalStaticVariables.ServerActions[action.Key]?.Invoke(GlobalStaticVariables.UdpClientList[clientInfo.sessionId], action.Value);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    
                }
            }

            _acceptThread = new Thread(Start);
            _acceptThread.Start();
        }
    }
}