using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using UnitySocketShared.Base.Classes;
using UnitySocketShared.Base.Global;

namespace UnitySocketServer.Network
{
    public static class TcpSocketAuth
    {
        public static Socket MainSocket;
        private static Thread _acceptThread;
        private static readonly object ThreadLockObject = new object();

        public static void Init()
        {
            Console.WriteLine("Tcp Socket Auth Init");
            
            MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            MainSocket.Bind(new IPEndPoint(IPAddress.Any, GlobalStaticVariables.TcpPort));
            MainSocket.Listen((int) SocketOptionName.MaxConnections);

            void Start(object _)
            {
                while (true)
                {
                    try
                    {
                        var clientSocket = MainSocket.Accept();

                        lock (ThreadLockObject)
                        {
                            var socketRemoteEndPoint = (IPEndPoint) clientSocket.RemoteEndPoint;
                            var clientIp = socketRemoteEndPoint.Address.ToString();
                            var clientPort = socketRemoteEndPoint.Port;
                            string sessionId;
                            using (var sha1 = new SHA1CryptoServiceProvider()) sessionId = Convert.ToBase64String(sha1.ComputeHash(Encoding.ASCII.GetBytes(DateTime.Now.ToString("yyyyMMddHHmmss") + clientIp + clientPort)));

                            var clientInfo = new ClientInfo
                            {
                                sessionId = sessionId,
                                connected = true,
                                datas = new Dictionary<string, object>(),
                                ip = clientIp,
                                port = clientPort,
                                socket = clientSocket,
                                pType = ProtocolType.Tcp
                            };

                            GlobalStaticVariables.TcpClientList.Add(sessionId, clientInfo);
                            Console.WriteLine("[TCP] Add Client - " + sessionId);
                            
                            GlobalStaticVariables.TcpClientList[sessionId].SendData();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    
                    Thread.Sleep(10);
                }
            }

            _acceptThread = new Thread(Start);
            _acceptThread.Start();
        }
    }
}