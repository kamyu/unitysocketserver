﻿using System;
using System.Net.Mime;
using System.Text;
using System.Threading;
using UnitySocketServer.Base.Command;

namespace UnitySocketServer
{
    internal static class Program
    {
        public static void Main()
        {
            // 콘솔 설정
            Console.Title = "Unity Server Console";
            Console.OutputEncoding = Encoding.UTF8;
            
            // 서버를 연다
            Network.TcpSocketAuth.Init();
            Network.TcpSocketReceive.Init();
            Network.UdpSocket.Init();
            
            // 소켓 리시브 액션을 설정한다
            Network.SocketActions.Init();
            
            // 콘솔 명령어를 입력한다
            ConsoleCommand.Init();
        }
    }
}
