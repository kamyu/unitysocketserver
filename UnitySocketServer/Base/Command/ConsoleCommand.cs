using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Net.Mime;
using System.Threading;

namespace UnitySocketServer.Base.Command
{
    public class ConsoleCommand
    {
        private static Thread _thread;
        private static string _inputString;
        private static readonly Dictionary<string, KeyValuePair<string, Action<List<string>>>> CommandDictionary = new Dictionary<string, KeyValuePair<string, Action<List<string>>>>();
        
        public static void Init()
        {
            _thread = new Thread(_ =>
            {
                while (true)
                {
                    _inputString = Console.ReadLine();
                    if (_inputString == null) continue;
                    
                    _inputString = _inputString.ToLower();
                    RunCommand(_inputString);
                    _inputString = null;
                }
            });
            _thread.Start();
            
            AddCommand("help", "Prints a list of all commands.", args =>
            {
                Console.WriteLine("=============================");
                foreach (var commandData in CommandDictionary)
                {
                    Console.WriteLine(string.Concat(new object[]
                    {
                        commandData.Key,
                        " - ",
                        commandData.Value.Key
                    }));
                }
                Console.WriteLine("=============================");
            });
            
            AddCommand("shutdown", "Exit the console.", args => { Environment.Exit(1); });
        }

        // 명령어를 실행한다
        private static void RunCommand(string readLine)
        {
            var commandRead = readLine.Split(' ');
            if (commandRead.Length <= 0) return;

            var command = commandRead[0];
            if (!CommandDictionary.ContainsKey(command)) command = "help";
            
            var args = commandRead.ToList();
            args.RemoveAt(0);

            if (CommandDictionary[command].Value == null) return;
            CommandDictionary[command].Value.Invoke(args);
        }
        
        // 명령어를 추가한다
        public static void AddCommand(string command, string decs, Action<List<string>> action)
        {
            command = command.ToLower();
            if (CommandDictionary.ContainsKey(command)) CommandDictionary.Remove(command);
            CommandDictionary.Add(command, new KeyValuePair<string, Action<List<string>>>(decs, action));
        }
    }
}