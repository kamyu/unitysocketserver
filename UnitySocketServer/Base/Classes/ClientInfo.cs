using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using UnitySocketShared.Base.Global;

namespace UnitySocketShared.Base.Classes
{
    [Serializable]
    public class ClientInfo
    {
        public string sessionId;
        public string ip;
        public int port;
        [NonSerialized] public bool connected;
        [NonSerialized] public Socket socket;
        [NonSerialized] public ProtocolType pType;
        [NonSerialized] public Dictionary<string, object> datas;
        
        public void SendData(Dictionary<string, List<object>> sendDatas = null, bool sendLog = true)
        {
            var sendData = new Dictionary<string, object> {{"clientInfo", this}};
            if (sendDatas != null) sendData.Add("actions", sendDatas);

            // 데이터를 바이트로 변환후 압축하여 보낸다.
            var ms = new MemoryStream();
            var bf = new BinaryFormatter {Binder = new AllowAllAssemblyVersionsDeserializationBinder()};
            bf.Serialize(ms, sendData);
            var bytes = ms.ToArray();
            
            // 메모리 스트림 초기화
            Array.Clear(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            ms.Position = 0;
            ms.SetLength(0);
            
            Snappy.Sharp.Snappy.MaxCompressedLength(GlobalStaticVariables.DefaultByteSize);
            var snappyBytes = Snappy.Sharp.Snappy.Compress(bytes);
            
            using (var ds = new DeflateStream(ms, CompressionMode.Compress)) ds.Write(snappyBytes, 0, snappyBytes.Length);
            var deflateBytes = ms.ToArray();
            ms.Close();
            
            if(sendLog) Console.WriteLine("[" + (pType == ProtocolType.Tcp ? "TCP" : pType == ProtocolType.Udp ? "UDP" : "?") + "] Send(" + sessionId + ") - " + bytes.Length + "/" + snappyBytes.Length + "/" + deflateBytes.Length);
            
            switch (pType)
            {
                case ProtocolType.Tcp:
                    socket.Send(deflateBytes, SocketFlags.None);
                    break;
                case ProtocolType.Udp:
                    socket.SendTo(deflateBytes, new IPEndPoint(IPAddress.Parse(ip), port));
                    break;
            }
        }

        public void SetData(string name, object data)
        {
            object prevValue = null;
            if (datas.ContainsKey(name))
            {
                prevValue = datas[name];
                datas.Remove(name);
            }
            datas.Add(name, data);

            if (prevValue != data)
                SendData(new Dictionary<string, List<object>>
                {
                    {
                        "SetData", new List<object>
                        {
                            name,
                            data
                        }
                    }
                });
        }

        public T GetData<T>(string name)
        {
            if (!datas.ContainsKey(name)) return default;
            return (T) datas[name];
        }
    }
}