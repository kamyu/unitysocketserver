namespace UnitySocketShared.Base.Classes
{
    public class SyncGameObject
    {
        // object
        public string objectInstanceId = "";
        public string objectNetworkName = "";
        
        
        // transform
        public float positionX;
        public float positionY;
        public float positionZ;
        public float rotationX;
        public float rotationY;
        public float rotationZ;
        public float scaleX;
        public float scaleY;
        public float scaleZ;
    }
}