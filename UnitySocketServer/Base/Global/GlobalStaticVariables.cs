using System;
using System.Collections.Generic;
using UnitySocketShared.Base.Classes;

namespace UnitySocketShared.Base.Global
{
    public static class GlobalStaticVariables
    {
        public const int TcpPort = 9000;
        public const int UdpPort = 9001;
        public const int DefaultByteSize = 1024;
        
        public static readonly Dictionary<string, ClientInfo> TcpClientList = new Dictionary<string, ClientInfo>();
        public static readonly Dictionary<string, ClientInfo> UdpClientList = new Dictionary<string, ClientInfo>();

        public static readonly Dictionary<string, Action<ClientInfo, List<object>>> ServerActions = new Dictionary<string, Action<ClientInfo, List<object>>>();
        public static readonly Dictionary<string, object> ServerData = new Dictionary<string, object>();
    }
}